const express = require('express')
const app = express()

app.get('/', (req, res) => {
  let name = 'Micha'

  res.send(`Hello ${name}`)
})

app.listen(3000, () => {
  console.log('Server is up!')
})