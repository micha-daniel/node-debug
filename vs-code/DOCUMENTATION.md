# Debugging

## Normal behavior

Normally you run a command like `node app.js` (package.json script ___start___) to start your application. If you do so in this project and enter `http://localhost:3000/` you will see `Hello Micha` on the website.

## Debug normal express app

First you need to create a launch.json via the ___Debug___ Menu on the left of the project.

![Screenshot](../_gfx/Screenshot_25.png)

To realy use the power of VS Code Debugging we want to create a `launch.json` file. To do so, click on ___create a launch.json file___. It creates a folder `.vscode` with a launch.json inside with the following content.

```json
{
  // Use IntelliSense to learn about possible attributes.
  // Hover to view descriptions of existing attributes.
  // For more information, visit: https://go.microsoft.com/fwlink/?linkid=830387
  "version": "0.2.0",
  "configurations": [
    {
      "type": "node",
      "request": "launch",
      "name": "Launch Program",
      "skipFiles": [
        "<node_internals>/**"
      ],
      "program": "${workspaceFolder}\\vs-code\\app.js"
    }
  ]
}
```

On the left upper view there is now a menu point ___Launch Program___. Click on the green arrow and it will just startup the programm. You now should see this:

![Screenshot](../_gfx/Screenshot_26.png)

You can set breakpoints whereever you want to do so. Just click on the line-number and a red dot will appear. If you are stopped in a breakpoint you will see a yellow arrow as also the line is marked yellow. Just remeber, that like in the example the line marked is not yet executed. As a remember as long as the bottom row is orange you are in an active development state.

At the left you see ___Variables___ where you can see the current scope. Below there is ___Watch___ where you can double click and add an expression you want. It only needs to be a valid JavaScript expression. Below you can see the ___Call Stack___ where we currently are in. You also can see the ___Loaded Scripts___ (includes node_modules) and the currently set ___Breakpoints___.

Under the code you see the section ___Debug Console___. Under that there is a single line where you can execute the JavaScript you want.

If you edit your code it will not create a temporary file as in the Chrome DevTools. To just edit your code and do this not temporary but permanantely we are required to use a Hot-Reloading tool like nodemon.

## Automatically reattach the debugger

I will show this example with nodemon but it will work with every other tool that automatically restart the app after a change. To follow this example you are required to install nodemon globally via `npm i -g nodemon`. Now add a debug script with this content `nodemon --inspect app.js`.

Now we are required to add an further configuration. Open `launch.json` in the bottom right there is a huge blue button ___Add Configuration...___, click this. You now should see a list, select the point ___Node.js: Attach to Process___. There is a new configuration created.

If you now change in ___Debug___ to ___Attach to Process ID___ and run it you will see, that it is searching for a process. But as it is no Node.js App running there is no active process.

If you now first start the App via `nodemon --inspect app.js` (`npm run debug`) and rerun ___Attach to Process ID___, you will now see at least one process. Select the frist entry. Now you can make a breakpoint and it will fall right into it. But if you change something, nodemon will restart the whole application. But if you now reload the webpage you will see that it won't fall back into the breakpoint. Also the inditication from VS Code is gone, as the bottom bar is now blue instead of orange.

To fix this behavior in `launch.json` we need to change the configuration, so that it looks like this:

```json
{
    "type": "node",
    "request": "attach",
    "name": "Attach to Process ID",
    "processId": "${command:PickProcess}",
    "restart": true,
    "protocol": "inspector"
},
```

After that and you restart both nodemon and the debugger, the debugger after a restart of nodemon will automatically reattach.