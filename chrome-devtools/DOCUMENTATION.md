# Debugging

## Normal behavior

Normaly you run a command like `node app.js` (package.json script ___start___) to start your application. If you do so in this project and enter `http://localhost:3000/` you will see `Hello Micha` on the website.

## Debug in Chrome DevTools with initial breakpoint

To debug your node app, you want to run `node --inspect-brk app.js` (package.json script ___debug-brk___) . In the console you now should see something like this:

```
$ node --inspect-brk app.js
Debugger listening on ws://127.0.0.1:9229/0f356bbb-47b3-469d-b5f6-1955331cc0a4
For help, see: https://nodejs.org/en/docs/inspector
```

The app is now running, but waits on the very first line of code. This means, if you open `http://localhost:3000/` you will get an error.

Now open your Chromium based browser with this link: `chrome://inspect`. The name will depend on your browser. As an example the same page is in the vivaldi browser `vivaldi://inspect`. You now should see:

![Screenshot](../_gfx/Screenshot_23.png)

It is possible that you only see ___Open dedicated DevTools for Node___, if that is the case, click on this link. Else if you see in the list below your target click on ___inspect___. Now you should see how the app is on a breakpoint on the very first line:

![Screenshot](../_gfx/Screenshot_24.png)

This will now act like a normal web application. You have a console to test some commands and you can set breakpoints with clicking on the line-number. On the right there is the menu to navigate in the code on the current breakpoint. On the left you see the file structure. If you press F8 for further executing, you now can navigate to `http://localhost:3000/` and see `Hello Micha`.

Like in normal programming for a web application you can edit directly in the DevTools. As an example if you change the variable to `John Doe` and save via CTRL+S you will now see an yellow exclamation mark. This means this changes are only available for the current session. If you refresh your page you now will have the response `Hello John Doe`.

## Debug in Chrome DevTools without initial breakpoint

To debug your node app without initial breakpoint , you want to run `node --inspect app.js` (package.json script ___debug___). To the previous version you remove the part `-brk`.

Else it is handling like in the previous example. If you start the app and open the app via inspect, you should now see the DevTools but without an initial breakpoint. This means the server will start right after you send the command. After a short while in the DevTools on the right in tab ___Node___ the files are loading and you can navigate to `app.js`.

Set a breakpoint and you can do everything as described before.