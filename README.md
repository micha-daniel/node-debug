# Node.js Debugging

This project is only an example how to debug Node.js Apps via Chrome DevTools or Visual Studio Code.

# Documentation

To make things simple, the explenations how to are in each folder in __DOCUMENTATION.md__.

Chrome DevTools: [Documentation](https://gitlab.com/micha-daniel/node-debug/-/blob/master/chrome-devtools/DOCUMENTATION.md)  
VS Code: [Documentation](https://gitlab.com/micha-daniel/node-debug/-/blob/master/vs-code/DOCUMENTATION.md)

# Technologies used

- __Node.js__ - 14.5.0
- __NPM__ - 6.14.5
- __VS Code__ - 1.46.1
- __Google Chrome__ - 83.0.4103.116 (Stable)

# Maintenance

This project will only be maintained by the owner of this repository.